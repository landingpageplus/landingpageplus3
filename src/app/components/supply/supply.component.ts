import { Component, OnInit } from '@angular/core';
import { SupplyService } from '../../services/supply.service';

@Component({
  selector: 'app-supply',
  templateUrl: './supply.component.html',
  styleUrls: ['./supply.component.css']
})
export class SupplyComponent implements OnInit {

  constructor(public supplyService: SupplyService) { }

  ngOnInit() {
  }

}
