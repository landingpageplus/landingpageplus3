import { Component, OnInit } from '@angular/core';
import { DemandService } from '../../services/demand.service';

@Component({
  selector: 'app-demand',
  templateUrl: './demand.component.html',
  styleUrls: ['./demand.component.css']
})
export class DemandComponent implements OnInit {

  constructor(public demandService: DemandService) {

  }

  ngOnInit() {
  }

}
