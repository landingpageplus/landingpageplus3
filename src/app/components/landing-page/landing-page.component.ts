import { Component, OnInit } from '@angular/core';
import {NgbTooltipConfig} from '@ng-bootstrap/ng-bootstrap';
@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.css'],
  providers: [NgbTooltipConfig]
})
export class LandingPageComponent implements OnInit {

  constructor(config: NgbTooltipConfig) {

     config.placement = 'right';
    // config.triggers = 'click';
   }

  ngOnInit() {
  }

}
