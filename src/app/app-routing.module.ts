import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { DemandComponent } from './components/demand/demand.component';
import { SupplyComponent } from './components/supply/supply.component';
import { LandingPageComponent } from './components/landing-page/landing-page.component';

const routes: Routes = [
   { path: '', redirectTo: '/', pathMatch: 'full' },
  { path: 'landing', component: LandingPageComponent },
  { path: 'demand', component: DemandComponent },
  { path: 'supply', component: SupplyComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
